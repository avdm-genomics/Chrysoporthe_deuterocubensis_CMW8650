#!/bin/bash
augustus --strand=both --progress=true --gff3=on --uniqueGeneId=true --species=neurospora_crassa ../CMW8650_genome.fasta > CMW8650_annotation.gff3
