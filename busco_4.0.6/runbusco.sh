#!/usr/bin/env bash
echo "Downloading compressed genome..."
wget https://gitlab.com/avdm-genomics/Chrysoporthe_deuterocubensis_CMW8650/-/raw/master/CMW8650_genome.fasta.xz
echo "Uncompressing genome..."
xzcat *.xz > genome.fa
echo "Running busco..."
busco -i genome.fa -c 20 -m geno -l sordariomycetes_odb10 -f --update-data --out output
echo "...done"
echo "Cleaning up..."
rm -rf genome.fa
rm -rf *.xz
rm -rf busco_downloads
cp output/*.txt .
XZ_OPT=-9 tar cvfJ busco_sequences.txz output/run*/busco_sequences
rm -rf output
echo "...done"
